 import pandas as pd 
path = "./Exam/"  Coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=";")  

# how many unique customers 

print(Coffeebar.describe())  

# to show different foods

Food = Coffeebar.groupby("FOOD").describe() 
print(Coffeebar["FOOD"].unique())  

#  to show different drinks

 Drink = Coffeebar.groupby("DRINKS").describe()
 print (Coffeebar["DRINKS"].unique())  

# new column 'year' to make the plot 

Coffeebar['year'] = Coffeebar["TIME"].str[0:4]

  # plot Food

 graph_food= Coffeebar[['FOOD','year']].groupby('year', as_index=False).count() 
graph_food.plot(x='year', y='FOOD')  

# plot Drinks 

graph_drink = Coffeebar[['DRINKS','year']].groupby('year', as_index=False).count() 
graph_drink.plot(x='year', y='DRINKS')   

# probabilities
 # new column "hour" in Coffeebar

 Coffeebar['hour'] = Coffeebar["TIME"].str[11:19]
 Coffeebar = Coffeebar.fillna("Nothing")   

# we create a new DataFrame 

df_drink = pd.DataFrame() 
df_food = pd.DataFrame()

# new column in df_food and df_drink 'time' as index 

df_drink['time'] = Coffeebar['hour'].unique()
 df_food ['time'] = Coffeebar['hour'].unique()
 df_food = df_food.set_index('time') 
df_drink = df_drink.set_index('time')

 # new column Total_commandes to use it in calculation 

Total_commandes = Coffeebar.groupby(['hour'])['hour'].count()  

# calculate probabilities

 for item in Coffeebar['DRINKS'].unique(): 
    df_drink[item] = pd.Series(Coffeebar.loc[Coffeebar['DRINKS'] == item].groupby(['hour'])['DRINKS'].count()*100 / Total_commandes, index=df_drink.index) 


for item in Coffeebar['FOOD'].unique(): 
    df_food[item] = pd.Series(Coffeebar.loc[Coffeebar['FOOD'] == item].groupby(['hour'])['FOOD'].count()*100 / Total_commandes, index=df_food.index) 
    df_food = df_food.fillna(0)   

# Now we have the probability of each hour for each product in the DataFrame df_conso
