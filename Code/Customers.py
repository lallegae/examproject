import random as random 
import pandas as pd
path = "./ExamProject/"
  df_drink = pd.read_csv(path+"df_drink.csv", sep=";")
 df_food = pd.read_csv(path+"df_food.csv", sep=";") 
Coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=";")  

df_food = df_food.set_index('time')
 df_drink = df_drink.set_index('time')

## Part 2 ##
#have only the hour
time = Coffeebar["TIME"].str[11:19]

#create object customer
  class customer(object): 
    def __init__(self,ID,budget): 
        self.ID=ID 
        self.budget=budget   

#create functions to obtain the simulation
    def consoFood (self, price_food, df_food, time):  
        choice = random.randint(0,100) 
        list_food = list(df_food.columns) 
        prob =0 
        for kind_food in list_food: 
            prob += df_food[kind_food][time] 
            if choice <= prob: 
                type_food = kind_food 
                if not (kind_food =='Nothing'): 
                    self.budget -= price_food[type_food] 
                return type_food  

    def consoDrink(self,price_drink, df_drink, time): 
        choice = random.randint(0, 100) 
        list_drink = list(df_drink.columns) 
        prob = 0 
        for kind_drink in list_drink: 
            prob += df_drink[kind_drink][time] 
            if choice <= prob: 
                type_drink = kind_drink 
                if not (kind_drink == 'Nothing'): 
                    self.budget -= price_drink[type_drink] 
                return type_drink      

#create the different types of customer
class onetime(customer): 
    def __init__(self,ID): 
        customer.__init__(self,ID,100)   

class isTripAdvisor(onetime): 
    def __init__(self, ID): 
        customer.__init__(self, ID, 100) 
        self.tip = random.randint(1,10)

   class regular_return (customer): 
    def __init__(self,ID): 
        customer.__init__(self, ID,250)    

class hipsters (customer): 
    def __init__ (self,ID ): 
        customer.__init__(self,ID,500)

#define prices
    price_food = {"sandwich": 5, "cookie": 2, "muffin":3, "pie":3}

  price_drink =  {"soda":3, "frappucino": 4, "milkshake": 5, "water":2, "tea": 3, "coffee":3}   

## Part 3 ##

# list returning 

returning_cust = [] 
ID_cust= 0 
list_conso = [] 
i=0 
for i in range (1000): 
    ID_cust +=1 
    Hazard= random.randint(1,3) 
    if Hazard ==1: 
        H = hipsters("CID" + str(ID_cust)) 
        returning_cust.append(H) 
    else : 
        rr = regular_return("CID" + str(ID_cust)) 
        returning_cust.append(rr)


    ID_onet = 1001 
date_list = Coffeebar["TIME"]
 for date in date_list: 
        proba= random.randint(1,100)  

        if proba<=20 and (len(returning_cust) !=0): 
            ID_customer = random.choice(returning_cust) 
        elif 20< proba < 28: 
            ID_customer= isTripAdvisor( "CID" + str(ID_onet)) 
            ID_onet +=1 
        else : 
            ID_customer = onetime ('CID' + str(ID_onet)) 
            ID_onet +=1  

        ID_customer.consoDrink(price_drink,df_drink,date[11:19]) 
        ID_customer.consoFood(price_food,df_food,date[11:19])    


        list_conso.append({"TIME": date, 
                          "ID": ID_customer.ID, 
                          "DRINKS": ID_customer.consoDrink(price_drink,df_drink,date[11:19]), 
                          "FOOD": ID_customer.consoFood(price_food, df_food,date[11:19] )})  

#check if the customer has enough money
        if (ID_customer.budget < 10): 
            returning_cust.remove(ID_customer)  

#Dataframe of the simulation
Simulation = pd.DataFrame(list_conso, columns=["TIME", "ID", "DRINKS", "FOOD"])

# dataFrame about  history of customers 
conso_ID =
Simulation[['TIME','FOOD','DRINKS','ID']].groupby(['ID','TIME','FOOD','DRINKS']).size() 
conso_ID = conso_ID.reset_index()   

#  Plot over the daily income: not succeeded  

Simulation['DATE'] = Simulation["TIME"].str[0:11] 
daily_food = Simulation[['DATE', 'FOOD']].groupby(['DATE', 'FOOD']).size()
 daily_food = daily_food.reset_index() 
Daily_food = pd.DataFrame.rename(daily_food, columns={"DATE":"DATE","FOOD":"FOOD","0":"AMOUNT"})
  daily_income= pd.DataFrame() 
daily_income["DATE"]= Simulation['DATE']
 daily_income = daily_income.set_index('DATE') 
for key in price_food: 
    daily_income =  Daily_food['AMOUNT']['DATE'] * price_food[key].groupby(['DATE']).sum() 

